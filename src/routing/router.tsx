import { createBrowserRouter } from "react-router-dom";
import App from "../App";
import Home from "../pages/Home";
import Record from "../pages/Record";
import RecordingCheckList from "../pages/RecordingCheckList";
import AgentDetails from "../pages/AgentDetails";
import RecordingPractice from "../pages/RecordingPractise";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {
        index: true,
        element: <Home />,
      },
      {
        path: "/Home",
        element: <Home />,
        children: [{
          path: "/Home/RecordingChecklist",
          element: <RecordingCheckList/>
        },
        {
          path: "/Home/AgentDetails",
          element: <AgentDetails/>
        },
        {
          path: "/Home/RecordingPractice",
          element: <RecordingPractice/>
        }]
      },
      { path: "/record", element: <Record /> },
    ],
  },
]);

export default router;
