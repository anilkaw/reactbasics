import React from "react";
import { NavLink, Outlet } from "react-router-dom";
import classes from "./Home.module.css";
import { useEffect, useState } from "react";
import axios from "./../axiosConfig";

const Home: React.FC = () => {
  const [introVideoUrl, setIntroVideoUrl] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('/TrainingTerrace/GetSelfServicePortalDetails');
        setIntroVideoUrl(response.data.data.microsoftgraphdownloadUrl);
      } catch (err) {
        console.log(err);
      }
    };
    fetchData();
  }, []);

  return (
    <>
      Home Component
      <br />
      <br />
      {/* {introVideoUrl} */}
      <NavLink
        to="/Home/RecordingChecklist"
        className={({ isActive }) =>
          `${classes.navLink} ${isActive ? classes.active : ""}`
        }
      >
        RecordingChecklist
      </NavLink>
      <NavLink
        to="/Home/AgentDetails"
        className={({ isActive }) =>
          `${classes.navLink} ${isActive ? classes.active : ""}`
        }
      >
        AgentDetails
      </NavLink>
      <NavLink
        to="/Home/RecordingPractice"
        className={({ isActive }) =>
          `${classes.navLink} ${isActive ? classes.active : ""}`
        }
      >
        RecordingPractice
      </NavLink>
      <br />
      <br />
      <Outlet />
    </>
  );
};

export default Home;
