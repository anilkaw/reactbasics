import React from "react";
import { NavLink } from "react-router-dom";
import classes from "./MainNavigation.module.css";
import { useLocation } from 'react-router-dom';

const MainNavigation: React.FC = () => {
  const location = useLocation();

  return (
    <header className={classes.header}>
      <nav>
        <ul className={classes.list}>
          <li>
            <NavLink
              to="/Home"
              className={({ isActive }) =>
                isActive || location.pathname === '/' || location.pathname.includes('/Home') ? classes.active : undefined
              }
              end
            >
              Train
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/record"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
              end
            >
              Record
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default MainNavigation;
