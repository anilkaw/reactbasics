import React from 'react';
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import attLogo from "./../../assets/att-logo.png";
import newBotLogo from "./../../assets/new-bot-logo.png";
import classes from './Header.module.css';
import { useNavigate } from 'react-router-dom';

const Header: React.FC = () => {
  const navigate = useNavigate();

  function goToHome(){
    navigate('/');
  }
  
  return (
    <>
      <Grid container columns={{ xs: 12, sm: 12, md: 12 }}>
        <Grid
          item
          xs={12}
          sm={12}
          md={12}
          sx={{ backgroundColor: "#00ABEA", padding: 1, color: "#fff" }}
        >
          <Box
            sx={{
              display: "flex",
              gap: 2,
            }}
          >
            {/* style={{ height: "25px" }} */}
            <img alt="att-logo" src={attLogo} className={classes.basicHeight} />
            <img
              alt="new-bot-logo"
              src={newBotLogo}
              className={classes.basicHeight}
              onClick={goToHome}
            />
            <Box component="span" sx={{ fontSize: "18px" }}>
              The VOT
            </Box>
          </Box>
        </Grid>
      </Grid>
    </>
  );
}

export default Header;
