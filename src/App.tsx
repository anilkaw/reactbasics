import React from 'react';
import Header from "./pages/Header/Header";
import "./App.css";
import MainNavigation from './pages/MainNavigation/MainNavigation';
import { Outlet } from "react-router-dom";

const App: React.FC = () => {
  return (
    <>
      <Header/>
      <MainNavigation/>
      <main>
      <Outlet />
      </main>
    </>
  );
};

export default App;
